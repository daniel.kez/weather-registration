from enum import Enum

# Forecast descriptions from wunderground
precipitation_weather = [
    "Drizzle",
    "Rain",
    "Snow",
    "Snow Grains",
    "Ice Crystals",
    "Ice Pellets",
    "Hail",
    "Mist",
    "Rain Mist",
    "Rain Showers",
    "Snow Showers",
    "Snow Blowing Snow Mist",
    "Ice Pellet Showers",
    "Hail Showers",
    "Small Hail Showers",
    "Thunderstorm",
    "Thunderstorms and Rain",
    "Thunderstorms and Snow",
    "Thunderstorms and Ice Pellets",
    "Thunderstorms with Hail",
    "Thunderstorms with Small Hail",
    "Freezing Drizzle",
    "Freezing Rain",
    "Freezing Fog",
    "Small Hail",
    "Squalls",
    "Unknown Precipitation",
]

good_weather = [
    "Clear",
    "Partly Cloudy",
    "Scattered Clouds",
]


class Condition(Enum):
    BAD = -1
    UNKNOWN = 0
    GOOD = 1


class WeatherCondition:
    def __init__(self, **kwargs):
        self.current_temp = kwargs.get('current_temp')
        self.average_temp = kwargs.get('average_temp')
        self.weather_condition = kwargs.get('weather_condition')
        self.icon_url = kwargs.get('icon_url')

    # Determine the current condition based on the average temp, current_temp
    # and weather condition.
    def current_condition(self):
        # The current weather can contain descriptive words not
        # present in the precipitation_weather or good_weather lists
        if [s for s in precipitation_weather if s in self.weather_condition]:
            return Condition.BAD
        elif [s for s in good_weather if s in self.weather_condition]:
            return Condition.GOOD

        if self.current_temp - self.average_temp > 5:
            return Condition.GOOD
        elif self.average_temp - self.current_temp > 5:
            return Condition.BAD

        return Condition.UNKNOWN
