import json

from pynamodb.attributes import NumberAttribute, UnicodeAttribute
from pynamodb.models import Model

with open('config.json') as data_file:
    config = json.load(data_file)


class Location(Model):
    class Meta:
        table_name = config['DYNAMODB_LOCATION_TABLE']
        region = 'us-east-1'
        host = 'https://dynamodb.us-east-1.amazonaws.com'

    location_id = UnicodeAttribute(hash_key=True, null=False)
    latitude = NumberAttribute(null=False)
    longitude = NumberAttribute(null=False)
    population = NumberAttribute(null=False)

    def __iter__(self):
        for name, attr in self._get_attributes().items():
            yield name, attr.serialize(getattr(self, name))
