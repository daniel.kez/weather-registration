import json

from pynamodb.attributes import UnicodeAttribute
from pynamodb.models import Model

with open('config.json') as data_file:
    config = json.load(data_file)

class Email(Model):
    class Meta:
        table_name = config['DYNAMODB_EMAIL_TABLE']
        region = 'us-east-1'
        host = 'https://dynamodb.us-east-1.amazonaws.com'

    email = UnicodeAttribute(hash_key=True, null=False)
    location_id = UnicodeAttribute(null=False)

    def __iter__(self):
        for name, attr in self._get_attributes().items():
            yield name, attr.serialize(getattr(self, name))
