### Demo

A demo of the UI is available [**here**](http://weather-registration-static-content.s3-website-us-east-1.amazonaws.com/).

### Prerequisites

* [Node.js][nodejs] v8.2.1 or higher
* [Python][python] v3.6.3 + [PIP][pip]
* [Serverless][serverless] v1.7 or higher
* [AWS Free Tier] - Sign up for aws free tier to publish to S3
* [Wunderground] - Weather API - Sign up for an API key [**here**](https://www.wunderground.com/weather/api?MR=1)


### Getting Started
In this project I used [serverless](https://serverless.com) for my API. This toolkit
enables deploying and operating serverless (e.g. aws lamba function based) infastructures.
There are a number of benefits to doing so not least of which is scalability.


First, follow the [instructions](https://serverless.com/framework/docs/providers/aws/guide/credentials/)
to set up credentials for AWS. Given the timeline on this project I did not also implement
an offline mode.

Next, sign up for an API key with Wunderground [**here**](https://www.wunderground.com/weather/api?MR=1)
> Note this is only required to make the send-email functionality work.

Install dependencies. Here we are installing three dependencies: the client, api, and serverless packages.
```bash
git clone https://gitlab.com/daniel.kez/weather-registration.git weather-registration
cd weather-registration
npm install
virtualenv --python=/usr/local/bin/python3 venv # This may be different depending on your installation location
source ./venv/bin/activate
pip3 install -r requirements.txt

```

**Set up your config.**
* Edit `config.json` to match your email & password, wunderground api key, and unique static content bucket name.
* Edit `client/src/config.json` to match the deployed the deployed API endpoint

> Given the need to specify the API endpoint for the client, I recommend deploying the api first
(`npm run deploy-server`), updating the config file, and then deploying the client (`npm run start-client`)


Time to deploy! This will deploy both the api and the static content to s3.
```bash
npm run deploy                                 # General static content, deploy it, and deploy lambda functions
npm run seed-location-db                       # Seed the location db

```

The app should become available at [http://weather-registration-static-content.s3-website-us-east-1.amazonaws.com/](http://weather-registration-static-content.s3-website-us-east-1.amazonaws.com/)

> Replace the bucket name with your bucket's name. Note you may also need to change the region.

To deploy only the API and run the static content locally you can run the following
```bash
npm run deploy-server
npm run start-client

```

Once your application is deployed and you have at least one registrant use the following command to send
the associated emails.

> Note this may take a while as we intentially sleep 1 second between API calls
so that wunderground isn't over taxed.


```bash
npm run send-emails

```


The app should become available at [http://localhost:3000/](http://localhost:3000/).

# Credits
[**React Static Boilerplate**](https://github.com/kriasoft/react-static-boilerplate)

[**1000 Largest US Cities By Population With Geographic Coordinates, in JSON**]
    (https://gist.github.com/Miserlou/c5cd8364bf91000 Largest US Cities By Population With Geographic Coordinates, in JSONb2420bb29)

[**emailregex.com**](emailregex.com)
