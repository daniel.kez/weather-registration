import json

from datetime import date, timedelta
from time import sleep

import requests

from models.condition import WeatherCondition
from models.location import Location

with open('config.json') as data_file:
    config = json.load(data_file)


def get_current_conditions(location: Location) -> WeatherCondition:
    """

    :param location: Location of interest
    :return: The WeatherCondition for the given location
    """
    conditions_url = 'http://api.wunderground.com/api/%s/conditions/q/%f,%f.json ' \
                     % (config['API_KEY'], location.latitude, location.longitude)
    try:
        # For throttling. This is a naive way of limiting requests. In a real environment
        # I would likely use the requests-respectful package
        sleep(1)
        res = requests.get(conditions_url)
        res.raise_for_status()
        current_conditions = res.json()
    except:
        print('Unsuccessful request when fetching current conditions for %s' % location.location_id)

    average_temp = get_average_temperature(location)

    weather_conditions = WeatherCondition(
        current_temp=float(current_conditions.get('current_observation').get('temp_f')),
        weather_condition=current_conditions.get('current_observation').get('weather'),
        icon_url=current_conditions.get('current_observation').get('icon_url'),
        average_temp=average_temp,
    )
    return weather_conditions


def get_average_temperature(location: Location) -> float:
    """

    :param location: Location of interest
    :return: float average forecast temperature +/- 7 days
    """
    today = date.today()
    start_date = today - timedelta(days=7)
    end_date = today - timedelta(days=-7)

    planner_url = 'http://api.wunderground.com/api/%s/planner_%02d%02d%02d%02d/q/%f,%f.json ' \
                  % (
                      config['API_KEY'],
                      start_date.month, start_date.day,
                      end_date.month, end_date.day,
                      location.latitude, location.longitude
                  )
    try:
        # For throttling. This is a naive way of limiting requests. In a real environment
        # we would likely use the requests-respectful package or something similar.
        sleep(1)
        res = requests.get(planner_url)
        res.raise_for_status()
        planned_conditions = res.json()
    except:
        print("Unsuccessful request when fetching 14 day plan for %s" % location.location_id)

    try:
        avg_high_temp = float(planned_conditions.get('trip').get('temp_high').get('avg').get('F'))
        avg_low_temp = float(planned_conditions.get('trip').get('temp_low').get('avg').get('F'))
    except:
        print("Error retrieving the average temperatures for %s" % location.location_id)

    avg_temp = (avg_high_temp + avg_low_temp) / 2
    return avg_temp
