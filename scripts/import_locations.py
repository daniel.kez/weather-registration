import json

from models.location import Location

# Credit to https://gist.github.com/Miserlou/c5cd8364bf9b2420bb29 for initial list
with open('scripts/cities_100.json') as data_file:
    cities = json.load(data_file)

# Cities json is pre-sorted
locations = [
    Location(
        location_id=city.get('location_id'),
        population=int(city.get('population')),
        latitude=float(city.get('latitude')),
        longitude=float(city.get('longitude')),
    )
    for city in cities
]

# Batch write locations to dynamodb
with Location.batch_write() as batch:
    for location in locations:
        batch.save(location)
