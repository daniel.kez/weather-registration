import json
import logging
import re

from models.email import Email
from models.location import Location

# Credit to emailregex.com
email_regex = re.compile("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")


def post(event, context):
    """
        Endpoint to register an email & location. The email must
        be unique and valid.
    """
    data = json.loads(event['body'])
    response_headers = {
        'Access-Control-Allow-Origin': '*'
    }

    # Perform validation
    if 'email' not in data:
        logging.error('Validation Failed')
        return {'statusCode': 422,
                'headers': response_headers,
                'body': json.dumps({'error_message': 'Couldn\'t register - missing email.'})}
    if 'location_id' not in data:
        logging.error('Validation Failed')
        return {'statusCode': 422,
                'headers': response_headers,
                'body': json.dumps({'error_message': 'Couldn\'t register - missing location_id.'})}
    if email_regex.match(data['email']) is None:
        logging.error('Validation Failed')
        return {'statusCode': 422,
                'headers': response_headers,
                'body': json.dumps({'error_message': 'Couldn\'t register due to invalid email.'})}

    if Location.count(hash_key=data['location_id']) != 1:
        logging.error('Validation Failed - location_id is invalid. %s', data)
        return {'statusCode': 422,
                'headers': response_headers,
                'body': json.dumps({'error_message': 'Couldn\'t register due to location_id being invalid.'})}

    if Email.count(hash_key=data['email']) != 0:
        logging.error('Validation Failed - email is already registered. %s', data)
        return {'statusCode': 409,
                'headers': response_headers,
                'body': json.dumps({'error_message': 'Couldn\'t register as email is already registered.'})}

    # Save the email object
    email = Email(email=data['email'], location_id=data['location_id'])
    email.save()

    # Return the generated email
    return {
        'statusCode': 200,
        'headers': response_headers,
        'body': json.dumps(dict(email)),
    }


