import json

from models.location import Location


def get(event, context):
    """
    Get all locations in dynamodb. These are the top 100 us cities by population

    :return: list of locations
    """
    locations = []
    for loc in Location.scan():
        locations.append(dict(loc))

    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps(locations),
    }
