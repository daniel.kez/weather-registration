import smtplib
import json

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from driver.wunderground import get_current_conditions
from models.condition import Condition
from models.email import Email
from models.location import Location

with open('config.json') as data_file:
    config = json.load(data_file)


def main(event, context):
    categorized_emails = {
        Condition.BAD: [],
        Condition.UNKNOWN: [],
        Condition.GOOD: [],
    }
    seen_locations = {}
    for email in Email.scan():
        if not seen_locations.get(email.location_id):
            weather_condition = get_current_conditions(Location.get(hash_key=email.location_id))
            current_condition = weather_condition.current_condition()
            seen_locations.setdefault(email.location_id, weather_condition)
            categorized_emails[current_condition].append(email)
        else:
            current_condition = seen_locations.get(email.location_id).current_condition()
            categorized_emails[current_condition].append(email)

    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        print(config['GMAIL_USER'], config['GMAIL_PASSWORD'])
        server.login(config['GMAIL_USER'], config['GMAIL_PASSWORD'])

        for condition in categorized_emails:
            for email in categorized_emails[condition]:
                weather_condition = seen_locations[email.location_id]

                msg = _create_email_msg(email, weather_condition)

                server.sendmail(config['GMAIL_USER'], email.email, msg.as_string())

        server.close()
    except BaseException as e:
        return {
            'stateCode': 500,
            'body': 'Error sending email',
        }


    return {
        'statusCode': 200,
        'body': 'Successfully sent emails',
    }


def _create_email_msg(email, weather_condition):
    """

    :param email: Email type
    :param weather_condition: WeatherCondition
    :return: MIMEMultipart message to email
    """
    condition = weather_condition.current_condition();

    subject_text = {
        Condition.BAD: "Not so nice out? That's okay, enjoy a discount on us.",
        Condition.UNKNOWN: "Enjoy a discount on us.",
        Condition.GOOD: "It's nice out! Enjoy a discount on us.",
    }

    # Create message container
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject_text[condition]
    msg['From'] = config['GMAIL_USER']
    msg['To'] = email.email

    # Create the message body (both plain-text and HTML).
    text = "Hi!\nIt's {} degrees and {} in {}!\n".format(
        weather_condition.current_temp,
        weather_condition.weather_condition,
        email.location_id,
    )
    msg.attach(MIMEText(text, 'plain'))

    html = """\
        <html>
          <head></head>
          <body>
            <p>Hi!<br>
               It's {} degrees and {} in {}!<br>
               <img src="{}">
            </p>
          </body>
        </html>
    """.format(
        weather_condition.current_temp,
        weather_condition.weather_condition,
        email.location_id,
        weather_condition.icon_url
    )
    msg.attach(MIMEText(html, 'html'))

    return msg
