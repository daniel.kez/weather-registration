import React from 'react';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';

// Simple confirmation page displaying a check
export default props => (
  <Grid>
    <Row>
      <Col>
        <Glyphicon glyph="check" />
      </Col>
    </Row>
  </Grid>
);
