import React from 'react';
import { Panel } from 'react-bootstrap';
import RegisterForm from './RegisterForm';
import Confirmation from './Confirmation';
import config from './config.json';

// Normally I would set up a dev / production config files
// and have the API_URL determined based on the stage the code is
// executed in.
const API_URL = config.API_URL;

// Credit emailregex.com
const email_regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class App extends React.Component {
  constructor() {
    super();

    // Some defaults
    this.state = {
      header: 'Register for the daily weather newsletter!',
      locations: [],
      locationID: '',
      email: '',
      registered: false,
    };
  }

  // Load locations data
  componentDidMount() {
    // Get the locations list
    fetch(`${API_URL}/locations`)
      .then(response => response.json())
      .then(json => {
        this.setState({
          locations: json,
          locationID: json[0].location_id,
        });
      })
      .catch(err => {
        this.setState({
          error: 'Error communicating with API',
        });
      });
  }

  // Wrapper on register API function
  register() {
    fetch(`${API_URL}/register`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        location_id: this.state.locationID,
      }),
    })
      .then(res => res.json())
      .then(json => {
        if (json.error_message) {
          this.setState({
            error: json.error_message,
          });
        } else {
          this.setState({
            registered: true,
            header: 'Registered!',
          });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({
          error: 'Error communicating with API',
        });
      });
  }

  handleChangeEmail(event) {
    this.setState({
      email: event.target.value,
      validEmail: email_regex.test(event.target.value),
    });
  }

  handleChangeLocationID(event) {
    this.setState({ locationID: event.target.value });
  }

  render() {
    return (
      <Panel
        header={this.state.error || this.state.header}
        bsStyle={this.state.error ? 'danger' : 'primary'}
      >
        {this.state.registered ? (
          <Confirmation />
        ) : (
          <RegisterForm
            onSubmit={this.register.bind(this)}
            onChangeEmail={this.handleChangeEmail.bind(this)}
            onChangeLocationID={this.handleChangeLocationID.bind(this)}
            locations={this.state.locations}
            validEmail={this.state.validEmail}
          />
        )}
      </Panel>
    );
  }
}
