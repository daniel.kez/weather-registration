import React from 'react';
import { Button, FormControl, FormGroup, ControlLabel } from 'react-bootstrap';

const FieldGroup = ({ id, label, ...props }) => (
  <FormGroup controlId={id}>
    <ControlLabel>{label}</ControlLabel>
    <FormControl {...props} />
  </FormGroup>
);

export default props => (
  <form>
    <FieldGroup
      id="email"
      type="email"
      label="Email address"
      placeholder="Enter email"
      onChange={props.onChangeEmail}
    />
    <FormGroup controlId="location_id">
      <ControlLabel>Location</ControlLabel>
      <FormControl
        componentClass="select"
        placeholder="select"
        onChange={props.onChangeLocationID}
      >
        {props.locations
          .map(({ location_id }) => location_id)
          .sort()
          .map(location_id => (
            <option key={location_id} value={location_id}>
              {location_id}
            </option>
          ))}
      </FormControl>
    </FormGroup>
    <Button onClick={props.onSubmit} disabled={!props.validEmail}>
      Register
    </Button>
  </form>
);
